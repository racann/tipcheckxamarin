﻿using System;
using System.Linq;
using Xamarin.Forms;
namespace TipCheck
{
	public class StringHelper
	{
		public static string statusText { set; get; }

		public StringHelper()
		{

		}
		public static bool validateInput(string inputString, Page page)
		{
			bool isValid = false;
			string outputString = string.Empty;
			bool allDigits = true;


			if (string.IsNullOrWhiteSpace(inputString) == true)
			{
				page.DisplayAlert("Invalid input", "You left something empty", "OK");
				isValid = false;
				//allDigits = false;
			}
			else
			{
				var getCharactersQuery = from elm in inputString.ToCharArray() select elm;
				foreach (var e in getCharactersQuery)
				{
					if (char.IsLetter(e) == true)
					{

						allDigits = false;
					}

				}
				if (allDigits == false)
				{
					page.DisplayAlert("Invalid input", "Only use numbers and decimals for calculation", "OK");
				}
				else
				{
					isValid = true;
				}


			}


			return isValid;
		}

		public static string formatCalc(double? inputDouble)
		{
			if (inputDouble < 0)
			{
				return "-" + string.Format("{0:C2}", inputDouble);
			}
			else
			{
				return string.Format("{0:C2}", inputDouble);
			}

		}

		public static string formatPercent(double? inputDouble)
		{
			if (inputDouble < 0)
			{
				return "-" + string.Format("{0:P}", inputDouble);
			}
			else
			{
				return string.Format("{0:P}", inputDouble);

			}
		}
	}
}
