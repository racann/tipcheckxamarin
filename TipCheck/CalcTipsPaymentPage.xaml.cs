﻿using System;
using System.Collections.Generic;
using Core;
using Xamarin.Forms;

namespace TipCheck
{
	public partial class CalcPaymentTipsPage : ContentPage
	{
		public CalcPaymentTipsPage()
		{
			InitializeComponent();
		}

        async void OnCalcTip()
        {

            if (await this.DisplayAlert(
                    "Calculate Tip",
                    "Finish Calculation?",
                    "Yes",
                "No"))
            {

                if (StringHelper.validateInput(orderTotalText.Text, this) == true && StringHelper.validateInput(percentToTipText.Text, this) == true)
                {
                    tipTotalText.Text = TipChecker.calculatePaymentTip(double.Parse(orderTotalText.Text), double.Parse(percentToTipText.Text)).ToString();
                    
                    tipTotalText.IsVisible = true;

                   
                    //View.IsFocusedProperty.Equals(false);
                }





            }


        }
    }
}
