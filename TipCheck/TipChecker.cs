﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
namespace Core
{
	//comment
	public class TipChecker
	{
		public TipChecker()
		{
		}
		public static double calculatePaymentTip(double bill, double percent)
		{
			double finalTip = 0;
			double tippingPercent = percent * 0.01;
			finalTip = bill * tippingPercent;
			return finalTip;
		}
		public static double? calculateTipPercent(double? bill, double? tip)
		{
			double? percent = 0;
			percent = tip/bill;
			return percent;
		}
	}

	public class Order //: INotifyPropertyChanged
	{
		
		public int? orderId { set; get; }
		public DateTime? orderTime { set; get; } = DateTime.Now;
		public double? totalBill { set; get; } = 0.0;
		public double? totalPaid { set; get; } = 0.0;
		public double? tip { set; get; } = 0.0;
		public double? tipPercent { set; get; } = 0.0;
		public string DisplayText { set; get; } = string.Empty;
		//public event PropertyChangedEventHandler PropertyChanged;




		//public void changed()
		//{
		//	PropertyChanged = 
		//}



		public Order()
		{
			orderId = null;
			orderTime = null;
			totalPaid = null;
			totalBill = null;
			tip = null;
			tipPercent = null;
			DisplayText = "";

		}

		public void calculateTip()
		{
			
				this.tip = this.totalPaid - this.totalBill;
			    this.tipPercent = TipChecker.calculateTipPercent(this.totalBill, this.tip);
				this.DisplayText = "Paid";

		}
        public  double? calculateTipPercent()
		{
	         double? percent = 0;
	         percent = tip /totalBill;
	         return percent;
		}

		public Order(double bill)
		{
			this.totalBill = bill;
			this.totalPaid = 0.0;
			this.DisplayText = "Unpaid";
			this.tip = 0.0;
		}

		//TODO: Add method for deleting tips using tip ID

	}
	public class RunningTotal
	{
		public ObservableCollection<Order> totalOrders { set; get; } = new ObservableCollection<Order>();
		public double? totalSales { set; get; }
		public double? totalPaid { set; get; }
		public double? totalTips { set; get; }
		public double? totalPercents { set; get; } = 0.0;

		public void deleteOrder(Order o)
		{
			totalOrders.Remove(o);
		}
        public void deleteOrder(int id)
        {
			Order orderToDelete = new Order();
			foreach (Order order in this.totalOrders)
			{
				if (id == order.orderId)
				{
					orderToDelete = order;
				}
			}
			this.totalOrders.Remove(orderToDelete);
		}

		public void addTip(Order order)
		{
			order.orderId = this.totalOrders.Count + 1;
			totalOrders.Add(order);
		}

		public void calculateTotals()
		{
			this.totalSales = 0.0;
			this.totalPaid = 0.0;
			this.totalTips = 0.0;
			foreach (Order o in totalOrders)
			{
				totalSales += o.totalBill;
				totalPaid += o.totalPaid;
				o.calculateTip();
				o.calculateTipPercent();
				totalPercents += o.tipPercent;
			}
			this.totalTips = this.totalPaid - this.totalSales;

		}
		public double? calcAvgTip()
		{
			double? avgTip = 0.0;

			avgTip= this.totalTips / this.totalOrders.Count;

			return avgTip;
		}
       public double? calcAvgTipPercent()
		{
	      double? avgTipPercent = 0.0;

	      avgTipPercent = this.totalPercents / this.totalOrders.Count;

	     return avgTipPercent;
		}
	}
}
