﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace TipCheck
{
	public partial class RunningTotalPage : ContentPage
	{
		public RunningTotalPage()
		{
			InitializeComponent();
			setLabels();
		}
		public void setLabels()
		{
			if (App.runningTotal.totalOrders.Count > 0)
			{
				totalSalesLabel.Text = "Total Sales: " + StringHelper.formatCalc(App.runningTotal.totalSales);
				totalTipsLabel.Text = "Total Tips: " + StringHelper.formatCalc(App.runningTotal.totalTips);
				avgTipLabel.Text = "AVG Tip: " + StringHelper.formatCalc(App.runningTotal.calcAvgTip());
				//avgPercentLabel.Text = "Average Tip Percent: " + StringHelper.formatPercent(App.runningTotal.calcAvgTipPercent());


			}
			else
			{
				totalTipsLabel.Text = string.Empty;
				totalSalesLabel.Text = string.Empty;
			}
		}

		async void OnOrderDetails(object sender, EventArgs e)
		  {
				await Navigation.PushAsync(new OrderDetailsPage());
			}
		async void OnDeleteTip(object sender, EventArgs e)
		{
			if (await this.DisplayAlert(
					"Delete Tip",
				"Delete Tip? (This action cannot be undone.)",
					"Yes",
				"No"))
			{
				var id = (Xamarin.Forms.Button)sender;
				App.runningTotal.deleteOrder(int.Parse(id.CommandParameter.ToString()));
				//App.TotalOrders.RemoveAt(int.Parse(id.CommandParameter.ToString()) - 1);
				setLabels();

			}
		}

	}
}
