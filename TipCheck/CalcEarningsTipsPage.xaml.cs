﻿using System;
using System.Collections.Generic;
using Core;

using Xamarin.Forms;

namespace TipCheck
{
	public partial class CalcEarningsTipsPage : ContentPage
	{
		
		public CalcEarningsTipsPage()
		{
			InitializeComponent();
			checkForPastTips();

		}
		public void checkForPastTips()
		{
			if(App.runningTotal.totalOrders.Count > 0)
				totalTipsButton.IsEnabled = true;
		}

		async void OnCalcTip()
		{

			if (await this.DisplayAlert(
					"Calculate Tip",
					"Finish Calculation?",
					"Yes",
				"No"))
			{
				
				if (StringHelper.validateInput(orderTotalText.Text, this)==true && StringHelper.validateInput(orderAmountPaidText.Text, this)==true)
				{
                Order order = new Order(double.Parse(orderTotalText.Text));
                order.orderTime = DateTime.Now;
				order.totalPaid = double.Parse(orderAmountPaidText.Text);
                order.calculateTip();
					//App.runningTotal.totalOrders.Add(order);
					App.runningTotal.addTip(order);
	            App.runningTotal.calculateTotals();
				App.TotalOrders = App.runningTotal.totalOrders;
				//App.TotalTips.Add(StringHelper.formatCalc(order.tip));
				tipTotalText.Text = "Most Recent Tip: " + StringHelper.formatCalc(order.tip);
				tipPercentText.Text = " Percent: " + StringHelper.formatPercent(order.tipPercent);
				tipPercentText.IsVisible = true;
				tipTotalText.IsVisible = true;
                
				totalTipsButton.IsEnabled = true;
					orderTotalText.Text = string.Empty;
					orderAmountPaidText.Text = string.Empty;
					View.IsFocusedProperty.Equals(false);
				}





			}


		}
		async void OnTotalTips(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new RunningTotalPage());

		}
	}
}
