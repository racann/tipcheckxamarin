﻿using System.Collections.Generic;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using Xamarin.Forms.Xaml;
using Core;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace TipCheck
{
	public partial class App : Application
	{
		//public static RunningTotal runningTotal { set; get; }
		public static RunningTotal runningTotal { set; get; }
		public static ObservableCollection<Order> TotalOrders { set; get; }
		public App()
		{
			InitializeComponent();
			//TotalOrders = new List<Order>();
			runningTotal = new RunningTotal();
			MainPage = new NavigationPage(new MainPage());
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
