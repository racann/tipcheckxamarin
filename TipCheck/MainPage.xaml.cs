﻿using System;
using Xamarin.Forms;

namespace TipCheck
{
	public partial class MainPage : ContentPage
	{
		

		public MainPage()
		{
			InitializeComponent();
		}

        async void OnCalcEarningsTips(object sender, EventArgs e)
		{
           await Navigation.PushAsync(new CalcEarningsTipsPage ());
		}

        async void OnCalcPaymentTips(object sender, EventArgs e)
		{
          await Navigation.PushAsync(new CalcPaymentTipsPage ());
		}
	}
}